class Temperature
  def initialize(opts = {})
    @options = opts
  end

  def in_celsius
    if @options[:c] == nil
      return (@options[:f]-32) * (5/9.to_f)
    else
      return (@options[:c])
    end
  end

  def in_fahrenheit
    if @options[:f] == nil
      return (@options[:c] *(9/5.to_f) + 32)
    else
      return(@options[:f])
    end
  end

  def self.from_celsius(x)
    Temperature.new(:c => x)
  end

  def self.from_fahrenheit(x)
    Temperature.new(:f => x)
  end
end

class Celsius < Temperature
  def initalize temp
    Celcius.new(temp)
  end
end

class Fahrenheit < Temperature
  def initalize temp
    Fahrenheit.new(temp)
  end
end
