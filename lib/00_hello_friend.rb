class Friend
  def greeting(friend = nil)
    return "Hello!" if friend.nil?
    return "Hello, #{friend}!" 
  end
end
