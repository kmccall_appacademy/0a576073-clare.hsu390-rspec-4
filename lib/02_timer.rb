class Timer


  def seconds
    0
  end

  def seconds=(time)
    @seconds = time
  end

  def padded(num)
    num = num.to_s
    return "0#{num}" if num.length == 1
    "#{num}"
  end

  def time_string
    seconds = @seconds
    minutes = 0
    hours = 0

    if seconds == 0
      return "00:00:00"
    elsif seconds < 60
      return "00:00:#{padded(seconds)}"
    end

    while seconds > 59
      minutes += 1
      seconds -= 60
      if minutes > 59
        hours += 1
        minutes -= 60
      end
    end
    return "#{padded(hours)}:#{padded(minutes)}:#{padded(seconds)}"
  end
end
