class Book
  def title
    @title
  end
  
  def title=(title)
  new_arr = []
  special_words = ["and", "in", "the", "of", "a", "an"]
  @title = title.split.each_with_index do |el, idx|
    if idx == 0
      new_arr << el.capitalize
    elsif special_words.include?(el)
      new_arr << el
    else
      new_arr << el.capitalize
    end
  end
  @title = new_arr.join(" ")
  end
end
