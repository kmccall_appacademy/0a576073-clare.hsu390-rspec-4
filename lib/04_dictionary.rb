class Dictionary

  def initialize(opts = {})
    @opts = opts
  end

  def entries
    @opts
  end

  def add(opts)
    opts.is_a?(String) ? @opts.merge!(opts => nil) : @opts.merge!(opts)
  end

  def keywords
    @opts.keys.sort
  end

  def include?(key)
    @opts.has_key?(key)
  end

  def find(key)
    @opts.select {|word, definition| word.scan(key).join == key}
  end

  def printable
  opts_sorted = @opts.sort_by { |word, defin| word}
  opts_sorted.map{ |word, defin| "[#{word}] \"#{defin}\"\n" }.join
  end

end
